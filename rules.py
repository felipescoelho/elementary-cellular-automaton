# Rules for elementary cellular automaton
# file name: rules.py
# author: Luiz Felipe da S. Coelho
# email: luizfscoelho.92@gmail.com  
# date: 02.05.2020 -- dd.mm.yyyy

def rule30(inpt):
    return int((inpt[0] & ~inpt[1] & ~inpt[-1]) | (~inpt[0] & inpt[1]) |
               (~inpt[0] & inpt[-1]))

def rule58(inpt):
    return int((~inpt[0] & inpt[-1]) | (inpt[0] & ~inpt[1]))

def rule90(inpt):
    return int((~inpt[0] & inpt[-1]) | (inpt[0] & ~inpt[-1]))

def rule110(inpt):
    return int((~inpt[-1] & inpt[1]) | (inpt[-1] & ~inpt[1]) |
               (inpt[1] & ~inpt[0]))

def rule184(inpt):
    return int((inpt[0] & ~inpt[1]) | (inpt[-1] & inpt[1]))