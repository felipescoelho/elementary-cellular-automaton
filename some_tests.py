# TESTS FOR ELEMENTARY CELLULAR AUTOMATON

# file name: some_tests.py
# author: Luiz Felipe da S. Coelho
# email: luizfscoelho.92@gmail.com
# date: 02.05.2020 -- dd.mm.yyyy

import numpy as np
import rules as rules
import matplotlib.pyplot as plt
from math import floor
from random import randint as rand

# read three cells and apply some rule to describe the cells of the next generation.

array_len = 1000
# init = 4
init = rand(0, array_len)

# generate the initial state
initial_state = np.zeros((1, array_len), dtype=int)
initial_state[0, init] = 1

automaton_mat = np.zeros((array_len, array_len), dtype=int)
automaton_mat[0, 0:] = initial_state

# body
for i in range(1, array_len):
    improved = np.append(automaton_mat[i-1, -1], 
                         np.append(automaton_mat[i-1, 0:],
                                   automaton_mat[i-1, 0])) == 1
    for j in range(array_len):
        regressor = improved[np.arange(j, j+3, 1)]
        # print(i, j, regressor)
        automaton_mat[i, j] = rules.rule184(regressor)

# ploting
plt.imshow(automaton_mat, cmap='binary')
plt.show()

